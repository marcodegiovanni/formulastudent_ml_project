# Machine Learning Project: Cone detection for a Formula Student vehicle
This software was developed for the Machine Learning exam at the Università degli studi Roma 3 and consists of an inference system for detecting traffic cones, carried out through a Neaural network based on YOLOv5 and using CUDA.
This software has been conceived for use on an autonomous Formula Student racing car.

**Note:** The code under 'models' and 'util' are not written by me, but it's from _YOLOv5_ library that it's release by _GNU General Public License v3_, for more information [visit the Ultralytics website](https://ultralytics.com/yolov5)

## How to use
Install the requirements using PiP ( ```pip3 install -r requirements.txt``` ), then launch using ```python3 cone_detection.py```

## Available parameters
- weights: PyTorch model path (default: _640x32.pt_)
- source: Source to process, can be an image, video or a webcam (default: _test.jpg_)
- inf-size: Inference size (in pixels, default: 640px)
- conf-thres: Object confidence threshold, from 0.0 to 1.0 (default: _0.25_)
- iou-thres: IOU (Intersection Over Union) threshold for Non Max Suppression filter, from 0.0 to 1.0 (default: _0.45_)
- device: Select the dive for inference, can be the CPU ('cpu') or a CUDA device (0, 1, 2 ecc.) (default: _cpu_)
- view-result: Display the result via OpenCV
- verbose: Use verbose output

example: Make an inference of "cones.jpg" using CUDA device 0 and a confidence threshold of 50%:

```python3 cone_detection.py --source cones.jpg --conf-thres 0.5 --device 0 --view-result --verbose```

