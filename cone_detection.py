"""
Cone detection system for a Formula Student Driverless vehicle

(c) 2021 - Marco De Giovanni
"""

import argparse
import time
from threading import Thread

import cv2
import torch

from models.experimental import attempt_load
from utils.general import non_max_suppression
from utils.torch_utils import select_device

class InferenceEngine:

	def __init__(self, device, weights, img_size, conf_threshold, iou_threshold, verbose, augmentation):
		self.verbose = verbose
		self.device = select_device(device)
		self.half = self.device.type != 'cpu'
		
		if(self.verbose):
			print('Using device \"{}\"'.format(self.device))
		
		self.model = attempt_load(weights, map_location=self.device)
		   
		self.img_size = img_size + (img_size % int(self.model.stride.max()))
		if self.verbose:
			print('Image size: {}'.format(self.img_size))
		
		if self.half:
			self.model.half()
			if(self.verbose):
				print('Using FP16')
		
		self.conf_threshold = conf_threshold
		self.iou_threshold = iou_threshold
		self.augmentation = augmentation

	def forward(self, image):
		height_ratio, width_ratio, _ = image.shape
		height_ratio /= self.img_size
		width_ratio /= self.img_size

		inf_image = image[:, :, [2,1,0]]
		
		blob = torch.from_numpy(inf_image.transpose(2, 0, 1)).to(self.device)
		blob = blob.half() if self.half else blob.float()
		
		blob /= 255.0
		if blob.ndimension() == 3:
			blob = blob.unsqueeze(0)
		
		pred = self.model(blob, augment=self.augmentation)
		pred = non_max_suppression(pred[0], self.conf_threshold, self.iou_threshold, classes=None, agnostic=False)[0]

		if self.verbose:
			print('\nProcessing results:')
		
		processed_preds = []
		for p in pred:
			processed_pred = [ int(width_ratio*p[0]), int(height_ratio*p[1]), int(width_ratio*p[2]), int(height_ratio*p[3]), int(p[4]*100) ]
			processed_preds.append(processed_pred)
			if self.verbose:
				print('\t[{},{}] [{},{}]\tconfidence: {}%'.format(processed_pred[0], processed_pred[1], processed_pred[2], processed_pred[3], processed_pred[4]))

		return processed_preds

class MultithreadingInferenceFacade(Thread):

	def __init__(self, inference_engine):
		Thread.__init__(self)
		self._ie = inference_engine

	def import_image(self, image):
		self._image = image

	def export_preds(self):
		return self._preds

	def run(self):
		self._preds = self._ie.forward(self._image)

class ConeDetector:
	def __init__(self, device, weights, img_size=640, conf_threshold=0.25, iou_threshold=0.45, verbose=False, augmentation=False):
		self._img_size = img_size

		self._ie_left = InferenceEngine(device, weights, img_size, conf_threshold, iou_threshold, verbose, augmentation)
		self._ie_right = InferenceEngine(device, weights, img_size, conf_threshold, iou_threshold, verbose, augmentation)

	def forward(self, image):
		left_dnn = MultithreadingInferenceFacade(self._ie_left)
		right_dnn = MultithreadingInferenceFacade(self._ie_right)

		left_dnn.import_image(image[0:self._img_size, 0:self._img_size])
		right_dnn.import_image(image[0:self._img_size, self._img_size:(2*self._img_size)])

		left_dnn.start()
		right_dnn.start()

		left_dnn.join()
		right_dnn.join()

		preds = left_dnn.export_preds()

		for p in right_dnn.export_preds():
			preds.append([p[0]+640, p[1], p[2]+640, p[3]])

		return preds

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--weights', type=str, default='640x32.pt', help='PyTorch model path')
	parser.add_argument('--source', type=str, default='test.jpg', help='source (can be an image, a video or a webcam)')
	parser.add_argument('--img-size', type=int, default=640, help='inference size (in pixels)')
	parser.add_argument('--conf-thres', type=float, default=0.25, help='object confidence threshold')
	parser.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
	parser.add_argument('--device', default='cpu', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
	parser.add_argument('--view-result', action='store_true', help='display results')
	parser.add_argument('--verbose', action='store_true', help='verbose output')

	opt = parser.parse_args()
	print(opt)
	
	dnn = ConeDetector(opt.device, opt.weights, verbose=opt.verbose, conf_threshold=opt.conf_thres, iou_threshold=opt.iou_thres)
	
	if '.jpg' in opt.source or '.png' in opt.source or '.jpeg' in opt.source:
		image = cv2.imread(opt.source)
		
		t0 = time.time()
	
		pred = dnn.forward(image)

		print('Inference completed, {} cone detected in {}s'.format(len(pred), round(time.time()-t0, 2)))
	
		if opt.view_result:
			for p in pred:
				cv2.rectangle(image, (p[0], p[1]), (p[2], p[3]), (0,255,0), 2)

			cv2.imshow('Inference result', image)
			cv2.waitKey(0)
			cv2.destroyAllWindows()
	else:
		stream_source = int(opt.source) if opt.source.isnumeric() else opt.source
		capture = cv2.VideoCapture(stream_source)
		if capture.isOpened():
			while True:
				ret, frame = capture.read()

				if ret:
					frame = cv2.resize(frame, (2*opt.img_size, opt.img_size), interpolation = cv2.INTER_AREA)
					pred = dnn.forward(frame)
				
					if opt.view_result:
						for p in pred:
							cv2.rectangle(frame, (p[0], p[1]), (p[2], p[3]), (0,255,0), 2)
						cv2.imshow('stream {}'.format(stream_source), frame)
						if cv2.waitKey(1) == ord('q'):
							break
				else:
					print("Can't receiver frame (stream ended?)")
					break

			capture.release()
			cv2.destroyAllWindows()
		else:
			print('Error: cannot open stream {}'.format(stream_source))
